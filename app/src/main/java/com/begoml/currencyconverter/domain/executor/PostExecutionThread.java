package com.begoml.currencyconverter.domain.executor;


/**
 * This interface will update the UI.
 */
public interface PostExecutionThread {

    void post(Runnable runnable);
}
