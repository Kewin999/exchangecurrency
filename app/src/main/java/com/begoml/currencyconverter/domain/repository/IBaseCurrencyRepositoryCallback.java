package com.begoml.currencyconverter.domain.repository;

public interface IBaseCurrencyRepositoryCallback {

    void onError(Throwable throwable);
}
