package com.begoml.currencyconverter.domain.interactors;

import android.support.annotation.NonNull;

public interface ExchangeCurrencyInteractor extends Interactor {

    <I extends IExchangeCurrencyCallback> void loadCurrency(@NonNull I callback);

    <I extends IExchangeCurrencyCallback> void getCurrency(@NonNull I callback);
}
