package com.begoml.currencyconverter.domain.repository;

import com.begoml.currencyconverter.data.dto.ICurrencyDto;
import com.begoml.currencyconverter.domain.models.ICurrencyModel;

import java.util.Collection;

public interface ICurrencyRepositoryCallback extends IBaseCurrencyRepositoryCallback {

    <M extends ICurrencyModel> void onCurrencyListLoaded(Collection<M> modelCollection);
}
