package com.begoml.currencyconverter.domain.models;

public interface ICurrencyModel {

    void setNumCode(String numCode);

    void setCharCode(String charCode);

    void setNominal(int nominal);

    void setName(String name);

    void setValue(double value);

    String getNumCode();

    String getCharCode();

    int getNominal();

    String getName();

    double getValue();

    String getModelId();

    void setModelId(String modelId);
}
