package com.begoml.currencyconverter.domain.interactors;

import android.support.annotation.NonNull;

import com.begoml.currencyconverter.data.net.NetworkConnection;
import com.begoml.currencyconverter.domain.executor.PostExecutionThread;
import com.begoml.currencyconverter.domain.executor.ThreadExecutor;
import com.begoml.currencyconverter.domain.models.CurrencyModel;
import com.begoml.currencyconverter.domain.models.ICurrencyModel;
import com.begoml.currencyconverter.domain.repository.ICurrencyRepositoryCallback;
import com.begoml.currencyconverter.domain.repository.DataRepository;

import java.util.Collection;

public class ExchangeCurrencyInteractorImpl<C extends IExchangeCurrencyCallback> implements ExchangeCurrencyInteractor {

    private final DataRepository dataRepository;
    private final ThreadExecutor threadExecutor;
    private final PostExecutionThread postExecutionThread;

    private C callback;

    public ExchangeCurrencyInteractorImpl(DataRepository dataRepository, ThreadExecutor threadExecutor
            , PostExecutionThread postExecutionThread) {
        if (dataRepository == null || threadExecutor == null || postExecutionThread == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null!!!");
        }
        this.dataRepository = dataRepository;
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
    }

    @Override
    public <I extends IExchangeCurrencyCallback> void loadCurrency(@NonNull I callback) {
        this.callback = (C) callback;
        this.threadExecutor.execute(this);
    }

    @Override
    public void run() {
        dataRepository.getCurrencyFromNetwork(new ICurrencyRepositoryCallback() {
            @Override
            public <M extends ICurrencyModel> void onCurrencyListLoaded(Collection<M> modelCollection) {
                ICurrencyModel ruModel = getRuModel();
                modelCollection.add((M) ruModel);
                dataRepository.addRuModel(ruModel);
                postExecutionThread.post(() -> callback.onCurrencyListLoaded(modelCollection));
            }

            @Override
            public void onError(Throwable throwable) {
                getCurrency(callback);
                postExecutionThread.post(() -> callback.onError(throwable));

            }
        });
    }

    private ICurrencyModel getRuModel() {
        ICurrencyModel ruModel = new CurrencyModel();
        ruModel.setModelId("RUB");
        ruModel.setValue(1);
        ruModel.setNominal(1);
        ruModel.setCharCode("RUB");
        ruModel.setName("Российский рубль");
        return ruModel;
    }

    @Override
    public <I extends IExchangeCurrencyCallback> void getCurrency(@NonNull I callback) {
        this.callback = (C) callback;
        this.threadExecutor.execute(currencyRunnable);
    }

    private final Runnable currencyRunnable = new Runnable() {
        @Override
        public void run() {
            dataRepository.getCurrencyFromDisk(new ICurrencyRepositoryCallback() {
                @Override
                public <M extends ICurrencyModel> void onCurrencyListLoaded(Collection<M> modelCollection) {
                    postExecutionThread.post(() -> callback.onCurrencyListLoaded(modelCollection));
                }

                @Override
                public void onError(Throwable throwable) {
                    postExecutionThread.post(() -> callback.onError(throwable));
                }
            });
        }
    };
}
