package com.begoml.currencyconverter.domain.interactors;

import com.begoml.currencyconverter.domain.models.ICurrencyModel;

import java.util.Collection;

public interface IExchangeCurrencyCallback extends IBaseCallback {

    <M extends ICurrencyModel>void onCurrencyListLoaded(Collection<M> currencyCollection);
}
