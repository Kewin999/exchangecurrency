package com.begoml.currencyconverter.domain.interactors;

public interface IBaseCallback {

    void onError(Throwable throwable);
}
