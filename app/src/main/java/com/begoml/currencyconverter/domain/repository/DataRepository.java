package com.begoml.currencyconverter.domain.repository;

import com.begoml.currencyconverter.domain.models.ICurrencyModel;

public interface DataRepository {
    <C extends ICurrencyRepositoryCallback> void getCurrencyFromNetwork(final C callback);

    <C extends ICurrencyRepositoryCallback> void getCurrencyFromDisk(final C callback);

    void addRuModel(ICurrencyModel ruModel);
}
