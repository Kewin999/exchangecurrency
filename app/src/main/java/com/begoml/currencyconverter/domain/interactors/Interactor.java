package com.begoml.currencyconverter.domain.interactors;

public interface Interactor extends Runnable {

    void run();
}
