package com.begoml.currencyconverter.domain.executor;

public interface ThreadExecutor {

    void execute(final Runnable runnable);
}
