package com.begoml.currencyconverter.domain.models;

public class CurrencyModel implements ICurrencyModel {

    private String modelId;
    private String numCode;
    private String charCode;
    private int nominal;
    private String name;
    private double value;

    public CurrencyModel() {

    }

    @Override
    public String getModelId() {
        return modelId;
    }

    @Override
    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    @Override
    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    @Override
    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    @Override
    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String getNumCode() {
        return numCode;
    }

    @Override
    public String getCharCode() {
        return charCode;
    }

    @Override
    public int getNominal() {
        return nominal;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getValue() {
        return value;
    }
}
