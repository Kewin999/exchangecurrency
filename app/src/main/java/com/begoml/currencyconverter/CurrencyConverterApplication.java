package com.begoml.currencyconverter;

import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

public class CurrencyConverterApplication extends MultiDexApplication {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
}
