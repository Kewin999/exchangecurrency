package com.begoml.currencyconverter.data.cache.database;

import com.begoml.currencyconverter.data.dto.ICurrencyDto;

import java.util.Collection;

public interface DbHelper {

    /**
     * insert currency record
     */
    <M extends ICurrencyDto> void insertCurrency(M model);

    /**
     * get all currency records
     */
    <M extends ICurrencyDto> Collection<ICurrencyDto> getCurrency();

    /**
     * remove all records
     */
    void clearCache();

    /**
     * check has cache
     */
    boolean isCached();

    /**
     * close any open database object.
     */
    void close();
}
