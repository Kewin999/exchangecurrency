package com.begoml.currencyconverter.data.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;


/**
 * Network Connection class used to request data from the network.
 */
public class NetworkConnectionImpl implements Callable<String>, NetworkConnection {

    private static final String CONTENT_TYPE_LABEL = "Content-Type";
    private static final String CONTENT_TYPE_VALUE_JSON = "application/json; charset=utf-8";
    private static final String CONTENT_TYPE_VALUE_XML = "application/x-www-form-urlencoded";
    private static final String CONTENT_TYPE_VALUE_XML_TEXT = "text/xml;charset=UTF-8";
    private static final int CONNECT_TIME_OUT = 30000;
    private static final int READ_TIME_OUT = 30000;

    public static final String REQUEST_METHOD_GET = "GET";

    private URL url;
    private String requestVerb;
    private int responseCode = 0;
    private String response = "";

    private NetworkConnectionImpl(String url, String requestVerb) throws MalformedURLException {
        this.url = new URL(url);
        this.requestVerb = requestVerb;
    }

    public static NetworkConnectionImpl createGetRequest(String url) throws MalformedURLException {
        return new NetworkConnectionImpl(url, REQUEST_METHOD_GET);
    }


    private void connect() {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            setupConnection(urlConnection);

            responseCode = urlConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                response = getStringFromInputStream(urlConnection.getInputStream());
            } else {
                response = getStringFromInputStream(urlConnection.getErrorStream());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
    }

    private String getStringFromInputStream(InputStream inputStream) throws UnsupportedEncodingException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "windows-1251"));
        StringBuilder stringBuilderResult = new StringBuilder();

        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilderResult.append(line);
            }
            return stringBuilderResult.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    private void setupConnection(HttpURLConnection connection) throws IOException {
        if (connection != null) {
            connection.setRequestMethod(requestVerb);
            connection.setReadTimeout(READ_TIME_OUT);
            connection.setConnectTimeout(CONNECT_TIME_OUT);
            connection.setDoInput(true);
            connection.setRequestProperty(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_JSON);
            connection.setRequestProperty(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_XML);
            connection.setRequestProperty(CONTENT_TYPE_LABEL, CONTENT_TYPE_VALUE_XML_TEXT);
        }
    }

    @Override
    public String call() throws Exception {
        return requestCall();
    }

    public String requestCall() {
        connect();
        return response;
    }
}
