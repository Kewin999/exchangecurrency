package com.begoml.currencyconverter.data.cache.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.begoml.currencyconverter.data.dto.CurrencyDto;
import com.begoml.currencyconverter.data.dto.ICurrencyDto;

import java.util.ArrayList;
import java.util.Collection;

/**
 * {@link DbHelperImpl}  wrapper over sqlite
 */
public class DbHelperImpl extends SQLiteOpenHelper implements DbHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CurrencyConverter.db";


    public static class CurrencyEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NUM_CODE = "numCode";
        public static final String COLUMN_CHAR_CODE = "charCode";
        public static final String COLUMN_NOMINAL = "Nominal";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_VALUE = "Value";
        public static final String COLUMN_ID = "id";
    }

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + CurrencyEntry.TABLE_NAME + " (" +
                    CurrencyEntry._ID + " INTEGER PRIMARY KEY," +
                    CurrencyEntry.COLUMN_NUM_CODE + " TEXT," +
                    CurrencyEntry.COLUMN_CHAR_CODE + " TEXT," +
                    CurrencyEntry.COLUMN_NOMINAL + " TEXT," +
                    CurrencyEntry.COLUMN_NAME + " TEXT," +
                    CurrencyEntry.COLUMN_ID + " TEXT," +
                    CurrencyEntry.COLUMN_VALUE + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + CurrencyEntry.TABLE_NAME;

    public DbHelperImpl(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public <M extends ICurrencyDto> void insertCurrency(M model) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CurrencyEntry.COLUMN_NUM_CODE, model.getNumCode());
        values.put(CurrencyEntry.COLUMN_CHAR_CODE, model.getCharCode());
        values.put(CurrencyEntry.COLUMN_NOMINAL, model.getNominal());
        values.put(CurrencyEntry.COLUMN_NAME, model.getName());
        values.put(CurrencyEntry.COLUMN_VALUE, model.getValue());
        values.put(CurrencyEntry.COLUMN_ID, model.getDtoId());

        db.insert(CurrencyEntry.TABLE_NAME, null, values);
    }

    @Override
    public <M extends ICurrencyDto> Collection<ICurrencyDto> getCurrency() {
        Collection<M> entities = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(
                CurrencyEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        M entity;
        while (cursor.moveToNext()) {
            entity = (M) new CurrencyDto();
            entity.setNumCode(cursor.getString(cursor.getColumnIndexOrThrow(CurrencyEntry.COLUMN_NUM_CODE)));
            entity.setCharCode(cursor.getString(cursor.getColumnIndexOrThrow(CurrencyEntry.COLUMN_CHAR_CODE)));
            entity.setName(cursor.getString(cursor.getColumnIndexOrThrow(CurrencyEntry.COLUMN_NAME)));
            entity.setValue(cursor.getString(cursor.getColumnIndexOrThrow(CurrencyEntry.COLUMN_VALUE)));
            entity.setNominal(cursor.getString(cursor.getColumnIndexOrThrow(CurrencyEntry.COLUMN_NOMINAL)));
            entity.setDtoId(cursor.getString(cursor.getColumnIndexOrThrow(CurrencyEntry.COLUMN_ID)));
            entities.add(entity);
        }
        cursor.close();

        return (Collection<ICurrencyDto>) entities;
    }


    @Override
    public void clearCache() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + CurrencyEntry.TABLE_NAME);
    }


    @Override
    public boolean isCached() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(
                CurrencyEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
        return cursor.getCount() > 0;
    }
}
