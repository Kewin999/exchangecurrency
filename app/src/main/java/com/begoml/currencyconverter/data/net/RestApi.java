package com.begoml.currencyconverter.data.net;

public interface RestApi {

    <C extends NetworkCallback> void getCurrency(C callback);
}
