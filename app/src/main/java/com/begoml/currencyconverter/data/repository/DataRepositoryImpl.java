package com.begoml.currencyconverter.data.repository;

import com.begoml.currencyconverter.data.dto.ICurrencyDto;
import com.begoml.currencyconverter.data.mapper.DataMapper;
import com.begoml.currencyconverter.data.repository.datasource.DataSourceFactory;
import com.begoml.currencyconverter.data.repository.datasource.ICurrencyNetworkCallback;
import com.begoml.currencyconverter.data.repository.datasource.DataSource;
import com.begoml.currencyconverter.domain.models.ICurrencyModel;
import com.begoml.currencyconverter.domain.repository.ICurrencyRepositoryCallback;
import com.begoml.currencyconverter.domain.repository.DataRepository;

import java.util.Collection;

/**
 * {@link DataRepositoryImpl} for get data
 */
public class DataRepositoryImpl implements DataRepository {

    private static DataRepositoryImpl repository;

    private final DataSourceFactory dataSourceFactory;
    private final DataMapper dataMapper;

    public static synchronized DataRepositoryImpl getInstance(DataSourceFactory dataSourceFactory, DataMapper dataMapper) {
        if (repository == null) {
            repository = new DataRepositoryImpl(dataSourceFactory, dataMapper);
        }

        return repository;
    }

    public DataRepositoryImpl(DataSourceFactory dataSourceFactory, DataMapper dataMapper) {
        this.dataSourceFactory = dataSourceFactory;
        this.dataMapper = dataMapper;
    }


    @Override
    public <C extends ICurrencyRepositoryCallback> void getCurrencyFromNetwork(final C callback) {
        final DataSource dataSource = this.dataSourceFactory.createNetworkDataStore();
        dataSource.getCurrency(new ICurrencyNetworkCallback() {
            @Override
            public <M extends ICurrencyDto> void onCurrencyListLoaded(Collection<M> dtoCollection) {
                saveCurrencyToDisk(dtoCollection);
                setCurrency(callback, dtoCollection);
            }

            @Override
            public void onError(Throwable throwable) {
                callback.onError(throwable);
            }
        });
    }

    private <M extends ICurrencyDto> void saveCurrencyToDisk(Collection<M> dtoCollection) {
        final DataSource dataSource = this.dataSourceFactory.createDiskDataStore();
        dataSource.saveCurrency(dtoCollection);
    }

    @Override
    public <C extends ICurrencyRepositoryCallback> void getCurrencyFromDisk(final C callback) {
        final DataSource dataSource = this.dataSourceFactory.createDiskDataStore();
        if (dataSource.isCached()) {
            dataSource.getCurrency(new ICurrencyNetworkCallback() {
                @Override
                public <M extends ICurrencyDto> void onCurrencyListLoaded(Collection<M> dtoCollection) {
                    setCurrency(callback, dtoCollection);
                }

                @Override
                public void onError(Throwable throwable) {
                    callback.onError(throwable);
                }
            });
        } else {
            getCurrencyFromNetwork(callback);
        }
    }

    @Override
    public void addRuModel(ICurrencyModel ruModel) {
        final DataSource dataSource = this.dataSourceFactory.createDiskDataStore();
        dataSource.addRuModel(dataMapper.transform(ruModel));
    }

    private <C extends ICurrencyRepositoryCallback, D extends ICurrencyDto
            , M extends ICurrencyModel> void setCurrency(final C callback
            , Collection<D> dtoCollection) {
        Collection<M> models = (Collection<M>) dataMapper.transform(dtoCollection);
        callback.onCurrencyListLoaded(models);
    }
}
