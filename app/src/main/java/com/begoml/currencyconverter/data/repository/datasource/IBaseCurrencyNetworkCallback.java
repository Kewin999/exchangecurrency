package com.begoml.currencyconverter.data.repository.datasource;

public interface IBaseCurrencyNetworkCallback {

    void onError(Throwable throwable);
}
