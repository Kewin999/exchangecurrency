package com.begoml.currencyconverter.data.repository.datasource;

import android.content.Context;

import com.begoml.currencyconverter.data.cache.database.DbHelper;
import com.begoml.currencyconverter.data.cache.database.DbHelperImpl;
import com.begoml.currencyconverter.data.dto.ICurrencyDto;
import com.begoml.currencyconverter.domain.executor.ThreadExecutor;

import java.util.Collection;


public class CacheDataSourceImpl implements DataSource {

    private static CacheDataSourceImpl cacheDataSourceImpl;

    private final Context context;
    private final ThreadExecutor threadExecutor;

    public static synchronized CacheDataSourceImpl getInstance(Context context, ThreadExecutor threadExecutor) {
        if (cacheDataSourceImpl == null) {
            cacheDataSourceImpl = new CacheDataSourceImpl(context, threadExecutor);
        }
        return cacheDataSourceImpl;
    }

    private CacheDataSourceImpl(Context context, ThreadExecutor executor) {
        if (context == null || executor == null) {
            throw new IllegalArgumentException("Invalid null parameter");
        }
        this.context = context.getApplicationContext();
        this.threadExecutor = executor;
    }

    @Override
    public <C extends ICurrencyNetworkCallback> void getCurrency(C callback) {
        DbHelper dbHelperImpl = new DbHelperImpl(context);
        try {
            callback.onCurrencyListLoaded(dbHelperImpl.getCurrency());
        } finally {
            dbHelperImpl.close();
        }
    }

    @Override
    public <M extends ICurrencyDto> void saveCurrency(Collection<M> dtoCollection) {
        DbHelper dbHelperImpl = new DbHelperImpl(context);
        if (dbHelperImpl.isCached()) {
            dbHelperImpl.clearCache();
        }
        try {
            for (M obj : dtoCollection) {
                dbHelperImpl.insertCurrency(obj);
            }
        } finally {
            dbHelperImpl.close();
        }
    }

    @Override
    public boolean isCached() {
        DbHelper dbHelperImpl = new DbHelperImpl(context);
        boolean cached;
        try {
            cached = dbHelperImpl.isCached();
        } finally {
            dbHelperImpl.close();
        }
        return cached;
    }

    @Override
    public void addRuModel(ICurrencyDto transform) {
        DbHelper dbHelperImpl = new DbHelperImpl(context);

        try {
            dbHelperImpl.insertCurrency(transform);
        } finally {
            dbHelperImpl.close();
        }
    }
}
