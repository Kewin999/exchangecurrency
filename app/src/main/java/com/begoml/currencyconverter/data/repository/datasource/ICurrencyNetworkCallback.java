package com.begoml.currencyconverter.data.repository.datasource;

import com.begoml.currencyconverter.data.dto.ICurrencyDto;

import java.util.Collection;

public interface ICurrencyNetworkCallback extends IBaseCurrencyNetworkCallback {

    <M extends ICurrencyDto> void onCurrencyListLoaded(Collection<M> entityCollection);
}
