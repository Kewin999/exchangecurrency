package com.begoml.currencyconverter.data.repository.datasource;

import android.content.Context;
import android.support.annotation.NonNull;

import com.begoml.currencyconverter.data.net.RestApi;
import com.begoml.currencyconverter.data.net.RestApiImpl;

public class DataSourceFactoryImpl implements DataSourceFactory {

    private final Context context;
    private final DataSource dataSource;

    public DataSourceFactoryImpl(@NonNull Context context, @NonNull DataSource dataSource) {
        this.context = context.getApplicationContext();
        this.dataSource = dataSource;
    }

    @Override
    public DataSource createNetworkDataStore() {
        RestApi restApi = new RestApiImpl(this.context);
        return new NetworkDataStoreImpl(restApi, this.dataSource);
    }

    @Override
    public DataSource createDiskDataStore() {
        return dataSource;
    }
}
