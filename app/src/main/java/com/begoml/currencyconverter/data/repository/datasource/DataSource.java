package com.begoml.currencyconverter.data.repository.datasource;

import com.begoml.currencyconverter.data.dto.ICurrencyDto;

import java.util.Collection;

public interface DataSource {

    <C extends ICurrencyNetworkCallback> void getCurrency(C callback);

    <M extends ICurrencyDto> void saveCurrency(Collection<M> dtoCollection);

    default boolean isCached() {
        return false;
    }

    default void addRuModel(ICurrencyDto transform) {

    }
}
