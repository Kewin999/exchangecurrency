package com.begoml.currencyconverter.data.repository.datasource;

import com.begoml.currencyconverter.data.dto.ICurrencyDto;
import com.begoml.currencyconverter.data.net.RestApi;
import com.begoml.currencyconverter.data.net.NetworkCallback;

import java.util.Collection;


public class NetworkDataStoreImpl implements DataSource {

    private final RestApi restApi;
    private final DataSource dataSource;

    public NetworkDataStoreImpl(RestApi restApi, DataSource dataSource) {
        this.restApi = restApi;
        this.dataSource = dataSource;
    }

    @Override
    public <C extends ICurrencyNetworkCallback> void getCurrency(final C callback) {
        restApi.getCurrency(new NetworkCallback() {
            @Override
            public <M extends ICurrencyDto> void onCurrencyListLoaded(Collection<M> entityCollection) {
                callback.onCurrencyListLoaded(entityCollection);
            }

            @Override
            public void onError(Throwable throwable) {
                callback.onError(throwable);
            }
        });
    }

    @Override
    public <M extends ICurrencyDto> void saveCurrency(Collection<M> dtoCollection) {

    }
}
