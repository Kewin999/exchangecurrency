package com.begoml.currencyconverter.data.dto;

public interface ICurrencyDto {

    String getNominal();

    String getName();

    String getValue();

    String getCharCode();

    String getNumCode();

    void setNumCode(String numCode);

    void setCharCode(String charCode);

    void setValue(String value);

    void setName(String name);

    void setNominal(String nominal);

    String getDtoId();

    void setDtoId(String dtoId);
}
