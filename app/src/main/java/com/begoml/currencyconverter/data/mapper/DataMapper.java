package com.begoml.currencyconverter.data.mapper;

import com.begoml.currencyconverter.data.dto.CurrencyDto;
import com.begoml.currencyconverter.data.dto.ICurrencyDto;
import com.begoml.currencyconverter.domain.models.CurrencyModel;
import com.begoml.currencyconverter.domain.models.ICurrencyModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DataMapper {

    public DataMapper() {
    }

    /**
     * Transform a {@link ICurrencyDto} into an {@link ICurrencyModel}.
     *
     * @param dto to be transformed.
     * @return {@link ICurrencyModel} if valid {@link ICurrencyDto} otherwise null.
     */
    public <D extends ICurrencyDto, M extends ICurrencyModel> M transform(D dto) {
        M model = null;
        if (dto != null) {
            model = (M) new CurrencyModel();
            model.setCharCode(dto.getCharCode());
            model.setName(dto.getName());
            model.setNumCode(dto.getNumCode());
            model.setNominal(Integer.valueOf(dto.getNominal()));
            model.setValue(Double.valueOf(dto.getValue().replace(",", ".")));
            model.setModelId(dto.getDtoId());
        }
        return model;
    }

    /**
     * Transform a {@link ICurrencyDto} into an {@link ICurrencyModel}.
     *
     * @param model to be transformed.
     * @return {@link ICurrencyDto} if valid {@link ICurrencyModel} otherwise null.
     */
    public <D extends ICurrencyDto, M extends ICurrencyModel> D transform(M model) {
        D dto = null;
        if (model != null) {
            dto = (D) new CurrencyDto();
            dto.setCharCode(model.getCharCode());
            dto.setName(model.getName());
            dto.setNumCode(model.getNumCode());
            dto.setNominal(String.valueOf(model.getNominal()));
            dto.setValue(String.valueOf(model.getValue()));
            dto.setDtoId(model.getModelId());
        }
        return dto;
    }

    /**
     * Transform a Collection of {@link ICurrencyDto} into a Collection of {@link ICurrencyModel}.
     *
     * @param dtoCollection Collection to be transformed.
     * @return {@link ICurrencyModel} if valid {@link ICurrencyDto} otherwise null.
     */
    public <D extends ICurrencyDto, M extends ICurrencyModel> Collection<ICurrencyModel> transform(Collection<D> dtoCollection) {
        Collection<M> userList = new ArrayList<>(dtoCollection.size());
        ICurrencyModel model;
        for (D dto : dtoCollection) {
            model = transform(dto);
            if (model != null) {
                userList.add((M) model);
            }
        }

        return (Collection<ICurrencyModel>) userList;
    }

}
