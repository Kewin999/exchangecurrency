package com.begoml.currencyconverter.data.net;

public interface NetworkConnection {

    String requestCall();
}

