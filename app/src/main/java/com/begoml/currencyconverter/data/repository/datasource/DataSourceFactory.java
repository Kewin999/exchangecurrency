package com.begoml.currencyconverter.data.repository.datasource;

public interface DataSourceFactory {

    DataSource createNetworkDataStore();

    DataSource createDiskDataStore();
}
