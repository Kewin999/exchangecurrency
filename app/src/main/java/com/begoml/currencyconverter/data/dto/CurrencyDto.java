package com.begoml.currencyconverter.data.dto;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Valute", strict = false)
public class CurrencyDto implements ICurrencyDto {

    @Element(name = "NumCode", required = false)
    private String numCode;

    @Element(required = false, name = "CharCode")
    private String charCode;

    @Element(required = false, name = "Nominal")
    private String nominal;

    @Element(required = false, name = "Name")
    private String name;

    @Element(required = true, name = "Value")
    private String value;

    @Attribute(name = "ID")
    private String dtoId;

    public String getDtoId() {
        return dtoId;
    }

    public void setDtoId(String dtoId) {
        this.dtoId = dtoId;
    }

    @Override
    public String getNumCode() {
        return numCode;
    }

    @Override
    public String getCharCode() {
        return charCode;
    }

    @Override
    public String getNominal() {
        return nominal;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    @Override
    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    @Override
    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
