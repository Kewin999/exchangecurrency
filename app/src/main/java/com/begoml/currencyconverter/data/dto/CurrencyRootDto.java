package com.begoml.currencyconverter.data.dto;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(strict = false)
public class CurrencyRootDto {

    @Attribute(name = "Date", required = false)
    private String date;

    @ElementList(required = false, name = "Valute", inline = true)
    private List<CurrencyDto> entities;

    public List<CurrencyDto> getEntities() {
        return entities;
    }

    public String getDate() {
        return date;
    }
}