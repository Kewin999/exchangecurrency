package com.begoml.currencyconverter.data.net;

import android.content.Context;

import com.begoml.currencyconverter.data.dto.CurrencyRootDto;
import com.begoml.currencyconverter.data.exception.ConnectionException;
import com.begoml.currencyconverter.data.exception.NetworkException;
import com.begoml.currencyconverter.data.utils.NetworkUtils;


import org.simpleframework.xml.core.Persister;

public class RestApiImpl implements RestApi {

    public static final String API_BASE_URL = "http://www.cbr.ru/";
    public static final String CURRENCY_URL = API_BASE_URL + "scripts/XML_daily.asp";

    private final Context context;
    private Persister serializer;

    public RestApiImpl(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Parameters cannot be null!!!");
        }
        this.context = context.getApplicationContext();
        this.serializer = new Persister();
    }

    @Override
    public <C extends NetworkCallback> void getCurrency(C callback) {
        if (NetworkUtils.isThereInternetConnection(context)) {
            try {
                NetworkConnection networkConnection =
                        NetworkConnectionImpl.createGetRequest(CURRENCY_URL);
                String response = networkConnection.requestCall();
                CurrencyRootDto currencyRootDto = serializer.read(CurrencyRootDto.class, response);
                callback.onCurrencyListLoaded(currencyRootDto.getEntities());
            } catch (Exception e) {
                showConnectionError(callback);
            }
        } else {
            showNotInternetConnectionError(callback);
        }
    }

    private <C extends NetworkCallback> void showNotInternetConnectionError(C callback) {
        callback.onError(new NetworkException());
    }

    private <C extends NetworkCallback> void showConnectionError(C callback) {
        callback.onError(new ConnectionException());
    }
}
