package com.begoml.currencyconverter.data.net;

import com.begoml.currencyconverter.data.dto.ICurrencyDto;

import java.util.Collection;

public interface NetworkCallback {

    <M extends ICurrencyDto> void onCurrencyListLoaded(Collection<M> entityCollection);

    void onError(Throwable throwable);
}
