package com.begoml.currencyconverter.presentation.view.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;

import com.android.databinding.library.baseAdapters.BR;
import com.begoml.currencyconverter.R;
import com.begoml.currencyconverter.domain.models.ICurrencyModel;
import com.begoml.currencyconverter.presentation.other.EnumCurrency;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ConverterViewModel {

    private ICurrencyModel currencyFrom;
    private ICurrencyModel currencyTo;
    private String amountFrom;
    private String amountTo;
    private List<String> currencyFromList;
    private List<String> currencyToList;

    private List<ICurrencyModel> models;

    public ConverterViewModel() {
        this.currencyFromList = new ArrayList<>();
        this.currencyToList = new ArrayList<>();
    }

    @DrawableRes
    public int getImageResFrom() {
        int resId = EnumCurrency.valueOf(currencyFrom.getCharCode()).getResId();
        return resId == 0 ? R.drawable.img_photo_placeholder : resId;
    }

    @DrawableRes
    public int getImageResTo() {
        int resId = EnumCurrency.valueOf(currencyTo.getCharCode()).getResId();
        return resId == 0 ? R.drawable.img_photo_placeholder : resId;
    }


    public int getSelectedFromPosition() {
        return getCurrencyFromList().indexOf(getCurrencyNameFrom());
    }

    public int getSelectedToPosition() {
        return getCurrencyToList().indexOf(getCurrencyNameTo());
    }

    private String getCurrencyNameFrom() {
        return currencyFrom.getName();
    }

    private String getCurrencyNameTo() {
        return currencyTo.getName();
    }

    public void setCurrencyFromList(List<String> currencyFromList) {
        this.currencyFromList = currencyFromList;
    }

    public List<String> getCurrencyFromList() {
        return currencyFromList;
    }

    public void setCurrencyToList(List<String> currencyToList) {
        this.currencyToList = currencyToList;
    }

    public List<String> getCurrencyToList() {
        return currencyToList;
    }

    public void setCurrencyFrom(ICurrencyModel currencyFrom) {
        this.currencyFrom = currencyFrom;
    }

    public ICurrencyModel getCurrencyFrom() {
        return currencyFrom;
    }

    public void setCurrencyTo(ICurrencyModel currencyTo) {
        this.currencyTo = currencyTo;
    }

    public ICurrencyModel getCurrencyTo() {
        return currencyTo;
    }

    public void setAmountFrom(String amountFrom) {
        this.amountFrom = amountFrom;
    }

    public String getAmountFrom() {
        return amountFrom;
    }

    public void setAmountTo(String amountTo) {
        this.amountTo = amountTo;
    }

    public String getAmountTo() {
        return amountTo;
    }

    public void setModels(Collection<ICurrencyModel> models) {
        this.models = (List<ICurrencyModel>) models;
    }

    public List<ICurrencyModel> getModels() {
        return models;
    }
}
