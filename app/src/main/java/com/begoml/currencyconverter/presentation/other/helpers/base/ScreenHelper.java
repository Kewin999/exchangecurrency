package com.begoml.currencyconverter.presentation.other.helpers.base;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;

public interface ScreenHelper {

    void replaceFragment(@IdRes int containerId, Fragment fragment);

    Fragment getCurrentFragmentById();

    void onBackStackChange();

    void replaceFragment(Fragment fragment);

    void addRootFragment(@IdRes int containerId, Fragment fragment);

    void hideProgress();

    void showProgress();

    void onBackPressed();
}
