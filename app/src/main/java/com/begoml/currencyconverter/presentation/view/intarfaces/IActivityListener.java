package com.begoml.currencyconverter.presentation.view.intarfaces;


import com.begoml.currencyconverter.presentation.other.helpers.base.ScreenHelper;

public interface IActivityListener {
    <S extends ScreenHelper> S getActivityScreenHelper();
}
