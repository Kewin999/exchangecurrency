package com.begoml.currencyconverter.presentation.base.presenters;

import android.os.Bundle;

import com.begoml.currencyconverter.presentation.base.BaseMvpView;
import com.begoml.currencyconverter.presentation.other.exception.ExceptionFactory;


public class BasePresenter<V extends BaseMvpView> implements IBasePresenter {

    protected V view;

    public <View extends BaseMvpView> void attachView(View view) {
        this.view = (V) view;
    }

    @Override
    public void dettachView() {
        this.view = null;
    }

    @Override
    public boolean isViewAttached() {
        return view != null;
    }

    protected V getMvpView() {
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

    }

    @Override
    public void onCreateView() {

    }

    @Override
    public void onDestroy() {
        dettachView();
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {

    }

    public void setThrowable(Throwable throwable) {
        if (isViewAttached()) {
            getMvpView().hideProgress();
            getMvpView().showDlgMsg(getAnError(throwable));
        }
    }

    protected String getAnError(Throwable e) {
        return ExceptionFactory.create(e, getMvpView().getViewContext());
    }
}
