package com.begoml.currencyconverter.presentation.other.helpers.base;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import com.begoml.currencyconverter.R;
import com.begoml.currencyconverter.presentation.base.fragments.BaseFragment;

import java.lang.ref.WeakReference;

public abstract class ScreenHelperImpl<A extends AppCompatActivity> implements ScreenHelper {

    private static final String BACK_STACK_ROOT_TAG = "root_fragment";

    private WeakReference<A> activity;
    private FragmentManager fragmentManager;
    protected Toolbar toolbar;

    public ScreenHelperImpl(A appCompatActivity) {
        this.fragmentManager = appCompatActivity.getSupportFragmentManager();
        this.activity = new WeakReference<>(appCompatActivity);
    }

    protected FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    protected A getActivity() {
        return activity.get();
    }

    protected Context getActivityContext() {
        return activity.get();
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        replaceFragment(getContainerId(), fragment);
    }

    @Override
    public void replaceFragment(@IdRes int containerId, Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.addToBackStack(BACK_STACK_ROOT_TAG);
        setAnimations(ft);
        ft.replace(containerId, fragment);
        ft.commit();
    }

    @Override
    public void addRootFragment(@IdRes int containerId, Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(containerId, fragment);
        ft.commit();
        setTitle((BaseFragment) fragment);
    }

    protected void setAnimations(FragmentTransaction ft) {
        ft.setCustomAnimations(R.anim.enter_from_left,
                R.anim.exit_to_right,
                R.anim.enter_from_right,
                R.anim.exit_to_left);
    }

    @Override
    public Fragment getCurrentFragmentById() {
        return getFragmentManager().findFragmentById(getContainerId());
    }

    @IdRes
    protected int getContainerId() {
        return R.id.containerId;
    }

    @Override
    public void onBackStackChange() {
        Fragment currentFragment = getCurrentFragmentById();
        if (currentFragment instanceof BaseFragment) {
            setTitle((BaseFragment) currentFragment);
        }
    }

    protected void setTitle(@NonNull BaseFragment fragment) {
        String title;

        if (fragment.getTitleResId() != 0) {
            title = getActivityContext().getString(fragment.getTitleResId());
        } else if (!TextUtils.isEmpty(fragment.getTitleString())) {
            title = fragment.getTitleString();
        } else {
            title = "";
        }

        setTitle(title);
    }

    public void setTitle(String title) {
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }

    @Override
    public void hideProgress() {
        //TODO hide  global progress
    }

    @Override
    public void showProgress() {
        //TODO  show global progress
    }

    @Override
    public void onBackPressed() {
        getActivity().finish();
    }

    public boolean isLastFragmentInStack() {
        return getFragmentManager().getBackStackEntryCount() == 0;
    }

    private void removeFragmentFromBackStack() {
        getFragmentManager().popBackStackImmediate();
    }

}
