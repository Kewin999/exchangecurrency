package com.begoml.currencyconverter.presentation.base;

import android.content.Context;

public interface BaseMvpView {

    void showProgress();

    void hideProgress();

    void showDlgMsg(String error);

    Context getViewContext();
}
