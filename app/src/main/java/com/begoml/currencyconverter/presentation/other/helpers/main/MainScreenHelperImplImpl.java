package com.begoml.currencyconverter.presentation.other.helpers.main;

import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.begoml.currencyconverter.R;
import com.begoml.currencyconverter.presentation.other.helpers.base.ScreenHelperImpl;
import com.begoml.currencyconverter.presentation.view.activity.MainActivity;
import com.begoml.currencyconverter.presentation.view.fragments.ExchangeCurrencyFragment;

public class MainScreenHelperImplImpl<A extends MainActivity> extends ScreenHelperImpl<A> implements MainScreenHelper {

    private boolean doubleBackToExitPressedOnce = false;

    public MainScreenHelperImplImpl(A activity) {
        super(activity);
        initViews();
    }

    private void initViews() {
        initToolbar();
    }

    private void initToolbar() {
        toolbar = getActivity().findViewById(R.id.toolbarId);
        getActivity().setSupportActionBar(toolbar);
        changeStatusBarColor(R.color.colorPrimaryDark);
    }

    private void changeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getActivity().getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), color));
        }
    }

    @Override
    public void showExchangeCurrencyScreen() {
        addRootFragment(getContainerId(), ExchangeCurrencyFragment.newInstance());
    }

    @Override
    public void onBackPressed() {
        if (isLastFragmentInStack()) {
            checkDoubleBackToExitPressedOnce();
        } else {
            super.onBackPressed();
        }
    }

    private void checkDoubleBackToExitPressedOnce() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        } else {
            setDoubleBackToExitPressedOnce(true);
            showExitToast();
            new Handler().postDelayed(() -> setDoubleBackToExitPressedOnce(false),
                    getActivity().getResources().getInteger(R.integer.double_back_to_exit_pressed_once_delay_millis));
        }
    }

    private void showExitToast() {
        Toast.makeText(getActivity(), getActivity().getString(R.string.exit_text), Toast.LENGTH_SHORT).show();
    }

    private void setDoubleBackToExitPressedOnce(boolean doubleBackToExitPressedOnce) {
        this.doubleBackToExitPressedOnce = doubleBackToExitPressedOnce;
    }

}
