package com.begoml.currencyconverter.presentation.preseters.currencyexchange;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.ArrayAdapter;

import com.begoml.currencyconverter.domain.interactors.ExchangeCurrencyInteractor;
import com.begoml.currencyconverter.domain.interactors.IExchangeCurrencyCallback;
import com.begoml.currencyconverter.domain.models.ICurrencyModel;
import com.begoml.currencyconverter.presentation.base.presenters.BasePresenter;
import com.begoml.currencyconverter.presentation.other.utils.Utils;
import com.begoml.currencyconverter.presentation.view.intarfaces.CurrencyExchangeView;
import com.begoml.currencyconverter.presentation.view.viewmodel.ConverterViewModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ExchangeCurrencyPresenter<M extends ICurrencyModel, VM extends ConverterViewModel> extends BasePresenter<CurrencyExchangeView> implements IExchangeCurrencyPresenter {

    private final static String DEFAULT_AMOUNT = "0";

    private final ExchangeCurrencyInteractor currencyInteractor;

    private VM viewModel;


    public ExchangeCurrencyPresenter(ExchangeCurrencyInteractor exchangeCurrencyInteractor) {
        this.currencyInteractor = exchangeCurrencyInteractor;
        viewModel = (VM) new ConverterViewModel();
    }

    @Override
    public void onViewCreated(Bundle savedInstanceState) {
        super.onViewCreated(savedInstanceState);
        if (savedInstanceState == null) {
            loadCurrency();
        } else {
            getCurrency();
        }
    }

    private void loadCurrency() {
        getMvpView().showProgress();
        this.currencyInteractor.loadCurrency(getCallback());
    }

    private void getCurrency() {
        getMvpView().showProgress();
        this.currencyInteractor.getCurrency(getCallback());
    }

    private IExchangeCurrencyCallback getCallback() {
        return callback;
    }

    private final IExchangeCurrencyCallback callback = new IExchangeCurrencyCallback() {
        @Override
        public <M extends ICurrencyModel> void onCurrencyListLoaded(Collection<M> models) {
            getMvpView().hideProgress();
            setModels(models);
            initViewModel();
            setViewModel();
        }

        @Override
        public void onError(Throwable throwable) {
            setThrowable(throwable);
        }
    };

    @Override
    public void onRefreshListener() {
        getMvpView().hideRefresh();
        loadCurrency();
    }

    private <I extends ICurrencyModel> void setModels(Collection<I> models) {
        viewModel.setModels((Collection<ICurrencyModel>) models);
    }

    private void initViewModel() {
        viewModel.setAmountFrom(DEFAULT_AMOUNT);
        viewModel.setAmountTo(DEFAULT_AMOUNT);

        ICurrencyModel currencyFrom = viewModel.getModels().get(0);
        ICurrencyModel currencyTo = currencyFrom;
        for (ICurrencyModel model : viewModel.getModels()) {
            if (!model.getModelId().equals(currencyTo.getModelId())) {
                currencyTo = model;
                break;
            }
        }
        updateCurrencyLists(currencyFrom, currencyTo);
    }

    @Override
    public void onChangeCurrencyClick() {
        String tempAmount = viewModel.getAmountFrom();
        exchangeModels();
        resetAmounts();
        updateSpinnerPosition();
        new Handler().postDelayed(() -> getMvpView().setAmountFrom(tempAmount), 250);
    }

    private void exchangeModels() {
        ICurrencyModel model = viewModel.getCurrencyFrom();
        viewModel.setCurrencyFrom(viewModel.getCurrencyTo());
        viewModel.setCurrencyTo(model);
    }

    private void updateSpinnerPosition() {
        getMvpView().removeSpinnerListener();
        getMvpView().setSelectedFromPosition(viewModel.getSelectedFromPosition());
        getMvpView().setSelectedToPosition(viewModel.getSelectedToPosition());
        getMvpView().addSpinnerListener();
    }

    @Override
    public void onCurrencyFromItemSelected(int position) {
        ICurrencyModel currencyFrom = viewModel.getModels().get(position);
        ICurrencyModel currencyTo = viewModel.getCurrencyTo();

        if (currencyFrom.getModelId().equals(currencyTo.getModelId())) {
            for (ICurrencyModel model : viewModel.getModels()) {
                if (!model.getModelId().equals(currencyTo.getModelId())) {
                    currencyTo = model;
                    break;
                }
            }
        }
        onCurrencyItemSelected(currencyFrom, currencyTo);
        convertAmountTo();
    }

    @Override
    public void onCurrencyToItemSelected(int position) {
        ICurrencyModel currencyFrom = viewModel.getCurrencyFrom();
        ICurrencyModel currencyTo = viewModel.getModels().get(position);

        if (currencyFrom.getModelId().equals(currencyTo.getModelId())) {
            for (ICurrencyModel model : viewModel.getModels()) {
                if (!model.getModelId().equals(currencyFrom.getModelId())) {
                    currencyFrom = model;
                    break;
                }
            }
        }

        onCurrencyItemSelected(currencyFrom, currencyTo);
        convertAmountFrom();
    }

    @Override
    public void afterFromTextChanged(String working) {
        if (Utils.parseStringToDouble(working) == 0) {
            resetAmounts();
            setAmountTo(viewModel.getAmountTo());
        } else {
            viewModel.setAmountFrom(working);
            convertAmountTo();
        }
    }

    @Override
    public void afterToTextChanged(String working) {
        if (Utils.parseStringToDouble(working) == 0) {
            resetAmounts();
            setAmountFrom(null);
        } else {
            viewModel.setAmountTo(working);
            convertAmountFrom();
        }
    }

    private void resetAmounts() {
        viewModel.setAmountFrom(null);
        viewModel.setAmountTo(null);
    }

    private boolean canConvertAmounts() {
        if (Utils.parseStringToDouble(viewModel.getAmountFrom()) == 0
                && Utils.parseStringToDouble(viewModel.getAmountTo()) == 0) {
            return false;
        }
        return true;
    }

    private void onCurrencyItemSelected(ICurrencyModel currencyFrom, ICurrencyModel currencyTo) {
        viewModel.setCurrencyFrom(currencyFrom);
        viewModel.setCurrencyTo(currencyTo);
        updateUi();
    }

    private void convertAmountTo() {
        if (!canConvertAmounts()) {
            return;
        }

        double tempAmountFrom = Utils.parseStringToDouble(viewModel.getAmountFrom());

        double amountFrom = viewModel.getCurrencyFrom().getValue() / viewModel.getCurrencyFrom().getNominal();
        double amountTo = viewModel.getCurrencyTo().getValue() / viewModel.getCurrencyTo().getNominal();

        double amount = (amountFrom / amountTo) * tempAmountFrom;
        viewModel.setAmountTo(Utils.formatCurrency(amount));
        setAmountTo(viewModel.getAmountTo());
    }

    private void convertAmountFrom() {
        if (!canConvertAmounts()) {
            return;
        }

        double tempAmountTo = Utils.parseStringToDouble(viewModel.getAmountTo());

        double amountFrom = viewModel.getCurrencyFrom().getValue() / viewModel.getCurrencyFrom().getNominal();
        double amountTo = viewModel.getCurrencyTo().getValue() / viewModel.getCurrencyTo().getNominal();


        double amount = (amountTo / amountFrom) * tempAmountTo;
        viewModel.setAmountFrom(Utils.formatCurrency(amount));
        setAmountFrom(viewModel.getAmountFrom());
    }

    private void setAmountTo(String amountTo) {
        getMvpView().removeTextListener();
        getMvpView().setAmountTo(amountTo);
        getMvpView().addTextListener();
    }

    private void setAmountFrom(String amountTo) {
        getMvpView().removeTextListener();
        getMvpView().setAmountFrom(amountTo);
        getMvpView().addTextListener();
    }

    private void updateUi() {
        getMvpView().removeSpinnerListener();
        getMvpView().setImageResourceFrom(viewModel.getImageResFrom());
        getMvpView().setImageResourceTo(viewModel.getImageResTo());
        getMvpView().setSelectedFromPosition(viewModel.getSelectedFromPosition());
        getMvpView().setSelectedToPosition(viewModel.getSelectedToPosition());
        getMvpView().addSpinnerListener();
    }

    private void setViewModel() {
        getMvpView().setViewModel(viewModel);
    }

    private void updateCurrencyLists(ICurrencyModel currencyFrom, ICurrencyModel currencyTo) {
        List<String> currencyFromList = new ArrayList<>();
        viewModel.setCurrencyFromList(currencyFromList);

        viewModel.setCurrencyFrom(currencyFrom);
        viewModel.setCurrencyTo(currencyTo);

        for (ICurrencyModel model : viewModel.getModels()) {
            currencyFromList.add(model.getName());
        }

        List<String> currencyToList = new ArrayList<>();
        currencyToList.addAll(currencyFromList);

        viewModel.setCurrencyToList(currencyToList);
    }

}
