package com.begoml.currencyconverter.presentation.base.presenters;

import android.os.Bundle;

import com.begoml.currencyconverter.presentation.base.BaseMvpView;

public interface IBasePresenter {

    <View extends BaseMvpView> void attachView(View view);

    void dettachView();

    boolean isViewAttached();

    void onCreate(Bundle savedInstanceState);

    void onCreateView();

    void onDestroy();

    void onPause();

    void onResume();

    void onViewCreated(Bundle savedInstanceState);
}
