package com.begoml.currencyconverter.presentation.view.intarfaces;


import android.support.annotation.DrawableRes;

import com.begoml.currencyconverter.presentation.base.BaseMvpView;
import com.begoml.currencyconverter.presentation.view.viewmodel.ConverterViewModel;

import java.util.List;

public interface CurrencyExchangeView extends BaseMvpView {
    void hideRefresh();

    <VM extends ConverterViewModel> void setViewModel(VM viewModel);

    void updateAdapters(List<String> currencyFromList, List<String> currencyToList);

    void removeSpinnerListener();

    void addSpinnerListener();

    void addTextListener();

    void removeTextListener();

    void setAmountTo(String amountTo);

    void setAmountFrom(String amountFrom);

    void setSelectedFromPosition(int selectedFromPosition);

    void setSelectedToPosition(int selectedToPosition);

    void setImageResourceFrom(@DrawableRes int redId);

    void setImageResourceTo(@DrawableRes int redId);
}
