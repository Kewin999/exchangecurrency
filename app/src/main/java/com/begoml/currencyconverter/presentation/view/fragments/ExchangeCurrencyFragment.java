package com.begoml.currencyconverter.presentation.view.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.begoml.currencyconverter.R;
import com.begoml.currencyconverter.data.mapper.DataMapper;
import com.begoml.currencyconverter.data.repository.datasource.CacheDataSourceImpl;
import com.begoml.currencyconverter.data.executor.JobExecutor;
import com.begoml.currencyconverter.data.repository.DataRepositoryImpl;
import com.begoml.currencyconverter.data.repository.datasource.DataSourceFactory;
import com.begoml.currencyconverter.data.repository.datasource.DataSourceFactoryImpl;
import com.begoml.currencyconverter.data.repository.datasource.DataSource;
import com.begoml.currencyconverter.databinding.FragmentExchangeCurrencyBinding;
import com.begoml.currencyconverter.domain.executor.PostExecutionThread;
import com.begoml.currencyconverter.domain.executor.ThreadExecutor;
import com.begoml.currencyconverter.domain.interactors.ExchangeCurrencyInteractor;
import com.begoml.currencyconverter.domain.interactors.ExchangeCurrencyInteractorImpl;
import com.begoml.currencyconverter.domain.repository.DataRepository;
import com.begoml.currencyconverter.presentation.base.fragments.BaseFragment;
import com.begoml.currencyconverter.presentation.other.MainUIThread;
import com.begoml.currencyconverter.presentation.other.helpers.main.MainScreenHelperImplImpl;
import com.begoml.currencyconverter.presentation.other.utils.Utils;
import com.begoml.currencyconverter.presentation.preseters.currencyexchange.ExchangeCurrencyPresenter;
import com.begoml.currencyconverter.presentation.preseters.currencyexchange.IExchangeCurrencyPresenter;
import com.begoml.currencyconverter.presentation.view.intarfaces.CurrencyExchangeView;
import com.begoml.currencyconverter.presentation.view.viewmodel.ConverterViewModel;

import java.util.List;

public class ExchangeCurrencyFragment extends BaseFragment<IExchangeCurrencyPresenter, MainScreenHelperImplImpl, FragmentExchangeCurrencyBinding> implements CurrencyExchangeView {

    private ArrayAdapter<String> currencyFromAdapter;
    private ArrayAdapter<String> currencyToAdapter;

    public static ExchangeCurrencyFragment newInstance() {
        ExchangeCurrencyFragment fragment = new ExchangeCurrencyFragment();
        fragment.setTitleResId(R.string.currency_exchange_title);
        return fragment;
    }

    @Override
    protected MainScreenHelperImplImpl getScreeHelper() {
        return screenHelper;
    }

    @Override
    public View onCreateViewFragment(@NonNull View view) {
        initViews(view);
        return view;
    }

    private void initViews(@NonNull View rootView) {
        progressView = rootView.findViewById(R.id.fl_progress);
        binding.srpRefresh.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        binding.srpRefresh.setOnRefreshListener(() -> presenter.onRefreshListener());

        binding.setCallback(presenter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.onViewCreated(savedInstanceState);
    }

    @Override
    public int getInflateLayoutRes() {
        return R.layout.fragment_exchange_currency;
    }

    @Override
    protected IExchangeCurrencyPresenter createPresenter() {
        ThreadExecutor threadExecutor = JobExecutor.getInstance();
        PostExecutionThread postExecutionThread = MainUIThread.getInstance();

        DataSource dataSource = CacheDataSourceImpl.getInstance(getActivity(), threadExecutor);
        DataSourceFactory dataStoreFactory =
                new DataSourceFactoryImpl(getContext(), dataSource);

        DataMapper dataMapper = new DataMapper();
        DataRepository dataRepository = DataRepositoryImpl.getInstance(dataStoreFactory, dataMapper);
        ExchangeCurrencyInteractor interactor = new ExchangeCurrencyInteractorImpl(dataRepository, threadExecutor, postExecutionThread);

        return new ExchangeCurrencyPresenter(interactor);
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void hideRefresh() {
        binding.srpRefresh.setRefreshing(false);
    }

    @Override
    public <VM extends ConverterViewModel> void setViewModel(VM viewModel) {
        currencyFromAdapter = new ArrayAdapter<>(getContext()
                , R.layout.item_spinner_converter, viewModel.getCurrencyFromList());
        currencyToAdapter = new ArrayAdapter<>(getContext()
                , R.layout.item_spinner_converter, viewModel.getCurrencyToList());
        currencyFromAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencyToAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        binding.spCurrencyFrom.setAdapter(currencyFromAdapter);
        binding.spCurrencyTo.setAdapter(currencyToAdapter);

        setSelectedFromPosition(viewModel.getSelectedFromPosition());
        setSelectedToPosition(viewModel.getSelectedToPosition());

        addSpinnerListener();
        addTextListener();
    }

    @Override
    public void addTextListener() {
        binding.etConvertFrom.addTextChangedListener(amountFrom);
        binding.etConvertTo.addTextChangedListener(amountTo);
    }

    @Override
    public void removeTextListener() {
        binding.etConvertFrom.removeTextChangedListener(amountFrom);
        binding.etConvertTo.removeTextChangedListener(amountTo);
    }

    @Override
    public void setAmountTo(String amountTo) {
        binding.etConvertTo.setText(amountTo);
    }

    @Override
    public void setAmountFrom(String amountFrom) {
        binding.etConvertFrom.setText(amountFrom);
    }

    @Override
    public void setSelectedFromPosition(int selectedFromPosition) {
        binding.spCurrencyFrom.setSelection(selectedFromPosition);
    }

    @Override
    public void setSelectedToPosition(int selectedToPosition) {
        binding.spCurrencyTo.setSelection(selectedToPosition);
    }

    @Override
    public void setImageResourceFrom(int redId) {
        Utils.setImageResource(binding.ivCurrencyFrom, redId);
    }

    @Override
    public void setImageResourceTo(int redId) {
        Utils.setImageResource(binding.ivCurrencyTo, redId);
    }

    @Override
    public void updateAdapters(List<String> currencyFromList, List<String> currencyToList) {
        removeSpinnerListener();
        currencyFromAdapter.clear();
        currencyFromAdapter.addAll(currencyFromList);
        currencyFromAdapter.notifyDataSetChanged();
        currencyToAdapter.clear();
        currencyToAdapter.addAll(currencyToList);
        currencyToAdapter.notifyDataSetChanged();
        addSpinnerListener();
    }

    @Override
    public void removeSpinnerListener() {
        binding.spCurrencyFrom.setOnItemSelectedListener(null);
        binding.spCurrencyTo.setOnItemSelectedListener(null);
    }

    @Override
    public void addSpinnerListener() {
        binding.spCurrencyFrom.setOnItemSelectedListener(spinnerListenerFrom);
        binding.spCurrencyTo.setOnItemSelectedListener(spinnerListenerTo);
    }

    private AdapterView.OnItemSelectedListener spinnerListenerFrom = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            presenter.onCurrencyFromItemSelected(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    };

    private AdapterView.OnItemSelectedListener spinnerListenerTo = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            presenter.onCurrencyToItemSelected(position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> arg0) {
        }
    };

    private TextWatcher amountFrom = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable working) {
            presenter.afterFromTextChanged(working.toString());
        }
    };

    private TextWatcher amountTo = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable working) {
            presenter.afterToTextChanged(working.toString());
        }
    };

}

