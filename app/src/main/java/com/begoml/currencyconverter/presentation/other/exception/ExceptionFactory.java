package com.begoml.currencyconverter.presentation.other.exception;

import android.content.Context;
import android.support.annotation.Nullable;

import com.begoml.currencyconverter.R;
import com.begoml.currencyconverter.data.exception.ConnectionException;
import com.begoml.currencyconverter.data.exception.NetworkException;

public class ExceptionFactory {

    /**
     * Creates a String representing an error message.
     **/
    public static @Nullable
    String create(@Nullable Throwable e, Context context) {
        String message;

        if (e instanceof NetworkException) {
            message = context.getString(R.string.msg_exception_message_no_internet);
        } else if (e instanceof ConnectionException) {
            message = context.getString(R.string.msg_exception_message_bad_process);
        } else {
            message = context.getString(R.string.msg_exception_message_something_went_error);
        }

        return message;
    }
}
