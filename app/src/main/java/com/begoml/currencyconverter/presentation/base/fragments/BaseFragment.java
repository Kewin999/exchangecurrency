package com.begoml.currencyconverter.presentation.base.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.databinding.ViewDataBinding;

import com.begoml.currencyconverter.presentation.base.presenters.IBasePresenter;
import com.begoml.currencyconverter.presentation.other.helpers.base.ScreenHelper;
import com.begoml.currencyconverter.presentation.view.intarfaces.IActivityListener;


public abstract class BaseFragment<P extends IBasePresenter, S extends ScreenHelper, VDB extends ViewDataBinding> extends MvpFragment<P, S> {

    private static final String TITLE = "title";
    private static final String TITLE_STRING = "titleString";

    @StringRes
    protected int title = 0;
    protected String titleString = null;

    protected S screenHelper;

    protected IActivityListener activityListener;

    protected VDB binding;

    protected View progressView;

    protected abstract S getScreeHelper();

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        if (activity instanceof IActivityListener) {
            activityListener = (IActivityListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement IActivityListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getInflateLayoutRes(), container, false);
        onCreateViewFragment(binding.getRoot());
        return binding.getRoot();
    }

    public abstract View onCreateViewFragment(@NonNull View view);

    @LayoutRes
    public abstract int getInflateLayoutRes();

    @Nullable
    public String getTitleString() {
        return titleString;
    }

    public void setTitleString(@Nullable String titleString) {
        this.titleString = titleString;
    }

    @StringRes
    public int getTitleResId() {
        return title;
    }

    public void setTitleResId(@StringRes int idString) {
        this.title = idString;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TITLE, title);
        if (!TextUtils.isEmpty(titleString)) {
            outState.putString(TITLE_STRING, titleString);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        screenHelper = (S) activityListener.getActivityScreenHelper();
    }

    @Override
    public void showProgress() {
        getScreeHelper().showProgress();
    }

    @Override
    public void hideProgress() {
        getScreeHelper().hideProgress();
    }

    @Override
    public Context getViewContext() {
        return getContext();
    }
}
