package com.begoml.currencyconverter.presentation.preseters.currencyexchange;

import com.begoml.currencyconverter.presentation.base.presenters.IBasePresenter;
import com.begoml.currencyconverter.presentation.intarfaces.ConverterListener;

public interface IExchangeCurrencyPresenter extends IBasePresenter, ConverterListener {
    void onRefreshListener();
}
