package com.begoml.currencyconverter.presentation.base.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.begoml.currencyconverter.presentation.base.BaseMvpView;
import com.begoml.currencyconverter.presentation.base.presenters.IBasePresenter;
import com.begoml.currencyconverter.presentation.other.helpers.base.ScreenHelper;


public abstract class MvpFragment<P extends IBasePresenter, S extends ScreenHelper> extends Fragment implements BaseMvpView {

    protected P presenter;

    protected abstract P createPresenter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        presenter.attachView(this);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showDlgMsg(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }
}
