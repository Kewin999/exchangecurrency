package com.begoml.currencyconverter.presentation.intarfaces;

public interface ConverterListener {

    void onChangeCurrencyClick();

    void onCurrencyFromItemSelected(int position);

    void onCurrencyToItemSelected(int position);

    void afterFromTextChanged(String s);

    void afterToTextChanged(String s);
}
