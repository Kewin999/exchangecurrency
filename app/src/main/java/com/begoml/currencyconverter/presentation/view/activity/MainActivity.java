package com.begoml.currencyconverter.presentation.view.activity;

import android.os.Bundle;

import com.begoml.currencyconverter.R;
import com.begoml.currencyconverter.presentation.base.activity.MvpActivity;
import com.begoml.currencyconverter.presentation.other.helpers.base.ScreenHelper;
import com.begoml.currencyconverter.presentation.other.helpers.main.MainScreenHelper;
import com.begoml.currencyconverter.presentation.other.helpers.main.MainScreenHelperImplImpl;
import com.begoml.currencyconverter.presentation.preseters.main.IMainPresenter;
import com.begoml.currencyconverter.presentation.preseters.main.MainPresenter;
import com.begoml.currencyconverter.presentation.view.intarfaces.MainView;

public class MainActivity extends MvpActivity<IMainPresenter, MainScreenHelper> implements MainView {

    @Override
    protected IMainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected MainScreenHelper getScreenHelper() {
        return screenHelper;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        screenHelper = new MainScreenHelperImplImpl<>(this);
        setContentView(R.layout.activity_main);
        presenter.onCreate(savedInstanceState);
    }

    @Override
    public <S extends ScreenHelper> S getActivityScreenHelper() {
        return (S) screenHelper;
    }

    @Override
    public void showExchangeCurrencyScreen() {
        getScreenHelper().showExchangeCurrencyScreen();
    }
}
