package com.begoml.currencyconverter.presentation.other.utils;

import android.annotation.SuppressLint;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.widget.ImageView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Utils {

    public static double parseStringToDouble(String working) {
        if (TextUtils.isEmpty(working)) {
            return 0;
        }

        try {
            return Double.parseDouble(working.replace(',', '.').replace(" ", ""));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static String formatCurrency(double value) {
        DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols(Locale.getDefault());
        unusualSymbols.setDecimalSeparator('.');
        unusualSymbols.setGroupingSeparator(' ');

        DecimalFormat decimalFormat = new DecimalFormat("###,###.####", unusualSymbols);
        decimalFormat.setGroupingSize(3);

        return decimalFormat.format(value);
    }

    @SuppressLint("ResourceType")
    public static void setImageResource(ImageView imageView, @DrawableRes int idRes) {
        if (imageView != null && idRes > 0) {
            imageView.setImageResource(idRes);
        }
    }

}
