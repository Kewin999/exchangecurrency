package com.begoml.currencyconverter.presentation.other;

import android.os.Handler;
import android.os.Looper;

import com.begoml.currencyconverter.domain.executor.PostExecutionThread;

public class MainUIThread implements PostExecutionThread {

    private final Handler handler;

    private static class Holder {
        private static final MainUIThread MAIN_UI_THREAD = new MainUIThread();
    }

    public static MainUIThread getInstance() {
        return Holder.MAIN_UI_THREAD;
    }

    private MainUIThread() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        handler.post(runnable);
    }
}
