package com.begoml.currencyconverter.presentation.other.helpers.main;

import com.begoml.currencyconverter.presentation.other.helpers.base.ScreenHelper;

public interface MainScreenHelper extends ScreenHelper {
    void showExchangeCurrencyScreen();
}
