package com.begoml.currencyconverter.presentation.other;

import android.support.annotation.IdRes;

import com.begoml.currencyconverter.R;

public enum EnumCurrency {

    BYR(974),
    EUR(978, R.drawable.ic_eur),
    USD(840, R.drawable.ic_usd),
    RUB(643, R.drawable.ic_rub),
    RUR(810, R.drawable.ic_rub),
    BYN(933, R.drawable.ic_byn),
    DEM(276),
    AFA(4),
    ALL(8),
    DZD(12),
    ADP(20),
    AZM(31),
    ARS(32),
    AUD(36, R.drawable.ic_aud),
    ATS(40),
    BSD(44),
    BHD(48),
    BDT(50),
    AMD(51),
    BBD(52),
    BEF(56),
    BMD(60),
    BTN(64),
    BOB(68),
    BWP(72),
    BZD(84),
    SBD(90),
    BND(96),
    BGL(100),
    MMK(104),
    BIF(108),
    BYB(112),
    KHR(116),
    CAD(124),
    CVE(132),
    KYD(136),
    LKR(144),
    CLP(152),
    CNY(156),
    COP(170),
    KMF(174),
    CRC(188),
    HRK(191),
    CUP(192),
    CYP(196),
    CZK(203),
    DKK(208),
    DOP(214),
    ECS(218),
    SVC(222),
    ETB(230),
    ERN(232),
    EEK(233),
    FKP(238),
    FJD(242),
    FIM(246),
    FRF(250),
    DJF(262),
    GMD(270),
    GHC(288),
    GIP(292),
    GRD(300),
    GTQ(320),
    GNF(324),
    GYD(328),
    HTG(332),
    HNL(340),
    HKD(344),
    HUF(348),
    ISK(352),
    INR(356),
    IDR(360),
    IRR(364),
    IQD(368),
    IEP(372),
    ILS(376),
    ITL(380),
    JMD(388),
    JPY(392),
    KZT(398, R.drawable.ic_kzt),
    JOD(400),
    KES(404),
    KPW(408),
    KRW(410),
    KWD(414),
    KGS(417),
    LAK(418),
    LBP(422),
    LSL(426),
    LVL(428),
    LRD(430),
    LYD(434),
    LTL(440),
    LUF(442),
    MOP(446),
    MGF(450),
    MWK(454),
    MYR(458),
    MVR(462),
    MTL(470),
    MRO(478),
    MUR(480),
    MXN(484),
    MNT(496),
    MDL(498),
    MAD(504),
    MZM(508),
    OMR(512),
    NAD(516),
    NPR(524),
    NLG(528),
    ANG(532),
    AWG(533),
    VUV(548),
    NZD(554),
    NIO(558),
    NGN(566),
    NOK(578),
    PKR(586),
    PAB(590),
    PGK(598),
    PYG(600),
    PEN(604),
    PHP(608),
    PTE(620),
    GWP(624),
    TPE(626),
    QAR(634),
    ROL(642),
    RWF(646),
    SHP(654),
    STD(678),
    SAR(682),
    SCR(690),
    SLL(694),
    SGD(702),
    SKK(703),
    VND(704),
    SIT(705),
    SOS(706),
    ZAR(710),
    ZWD(716),
    ESP(724),
    SDD(736),
    SRG(740),
    SZL(748),
    SEK(752),
    CHF(756),
    SYP(760),
    TJR(762),
    THB(764),
    TOP(776),
    TTD(780),
    AED(784),
    TND(788),
    TRL(792),
    TMM(795),
    UGX(800),
    MKD(807),
    EGP(818),
    GBP(826),
    TZS(834),
    UYU(858),
    UZS(860),
    VEB(862),
    WST(882),
    YER(886),
    YUM(891),
    ZMK(894),
    TWD(901),
    XAF(950),
    XCD(951),
    XOF(952),
    XPF(953),
    XDR(960),
    TJS(972),
    AOA(973),
    BGN(975),
    CDF(976),
    BAM(977),
    UAH(980),
    GEL(981),
    PLN(985, R.drawable.ic_pln),
    BRL(986),
    XEU(954),
    BRK(987),
    AFN(971),
    SSP(728),
    CUC(931),
    ZWL(932),
    TMT(934),
    GHS(936),
    VEF(937),
    SDG(938),
    UYI(940),
    RSD(941),
    MZN(943),
    AZN(944),
    RON(946),
    CHE(947),
    CHW(948),
    TRY(949),
    SRD(968),
    MGA(969),
    COU(970),
    MXV(979),
    BOV(984),
    CLF(990),
    USN(997),
    USS(998),
    UNKNOWN(-1),;

    private int code;

    @IdRes
    private int resId = 0;

    EnumCurrency(int code, int resId) {
        this.code = code;
        this.resId = resId;
    }

    EnumCurrency(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @IdRes
    public int getResId() {
        return resId;
    }

    public static EnumCurrency valueOfString(String code) {
        try {
            int codeInt = Integer.parseInt(code);
            return valueOf(codeInt);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return valueOf(code);
    }

    public static EnumCurrency valueOf(int code) {
        for (EnumCurrency c : values()) {
            if (c.getCode() == code) {
                return c;
            }
        }

        return UNKNOWN;
    }
}
