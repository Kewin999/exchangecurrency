package com.begoml.currencyconverter.presentation.preseters.main;

import android.os.Bundle;

import com.begoml.currencyconverter.presentation.base.presenters.BasePresenter;
import com.begoml.currencyconverter.presentation.view.intarfaces.MainView;

public class MainPresenter extends BasePresenter<MainView> implements IMainPresenter {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getMvpView().showExchangeCurrencyScreen();

        }
    }
}
