package com.begoml.currencyconverter.presentation.base.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.begoml.currencyconverter.presentation.base.BaseMvpView;
import com.begoml.currencyconverter.presentation.base.presenters.IBasePresenter;
import com.begoml.currencyconverter.presentation.other.helpers.base.ScreenHelper;
import com.begoml.currencyconverter.presentation.view.intarfaces.IActivityListener;

public abstract class MvpActivity<P extends IBasePresenter, S extends ScreenHelper>
        extends AppCompatActivity implements BaseMvpView, IActivityListener, FragmentManager.OnBackStackChangedListener {

    protected P presenter;
    protected S screenHelper;

    protected abstract P createPresenter();

    protected abstract S getScreenHelper();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();
        presenter.attachView(this);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showDlgMsg(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getSupportFragmentManager().removeOnBackStackChangedListener(this);
    }

    @Override
    public void onBackStackChanged() {
        if (getScreenHelper() != null) {
            getScreenHelper().onBackStackChange();
        }
    }

    @Override
    public void onBackPressed() {
        getScreenHelper().onBackPressed();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public Context getViewContext() {
        return this;
    }
}
