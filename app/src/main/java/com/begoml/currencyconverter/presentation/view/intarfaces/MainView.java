package com.begoml.currencyconverter.presentation.view.intarfaces;

import com.begoml.currencyconverter.presentation.base.BaseMvpView;

public interface MainView extends BaseMvpView {
    void showExchangeCurrencyScreen();
}
